const jhdata = window.jhdata || {};
const logged_user = window.logged_user || "support";
const server_version = window.server_version || "";
const base_url = jhdata.base_url || "/";
const token = window.token || "";
// const xsrfToken = jhdata.xsrf_token;

export const jhapiRequest = (endpoint, method, data) => {
  let api_url = `${base_url}hub/api`;
  let suffix = "";
  // Starting from JupyterHub 4.1.5, _xsrf tokens are created for each sub path. Thus,
  // our service has a different _xsrf token compared to path /hub/. When we make
  // these API requests, the browser will send _xsrf token corresponding to /hub/ path
  // as requested API call is to path /hub/api/(.*). But the _xsrf token for the service
  // is different and hence, the request fails due to mismatched _xsrf tokens.
  //
  // Now we are using OAuth tokens and we will pass to browser via window data.
  // So we will add a Authorization header to the request and server will first check
  // this header and attempt to serve the request based on the scopes available on 
  // this token
  //
  // if (xsrfToken) {
  //   // add xsrf token to url parameter
  //   var sep = endpoint.indexOf("?") === -1 ? "?" : "&";
  //   suffix = sep + "_xsrf=" + xsrfToken;
  // }
  let headers = {
    "Content-Type": "application/json",
    "X-Idris-Logged-User": logged_user,
    "X-JupyterHub-Version": server_version,
    Accept: "application/jupyterhub-pagination+json",
  };

  // Add Authorization header
  if (token) {
    headers["Authorization"] = `token ${token}`;
  }
  return fetch(api_url + endpoint + suffix, {
    method: method,
    json: true,
    headers: headers,
    body: data ? JSON.stringify(data) : null,
  });
};
