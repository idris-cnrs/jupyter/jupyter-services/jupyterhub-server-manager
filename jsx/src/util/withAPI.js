import { withProps } from "recompose";
import { jhapiRequest } from "./jhapiUtil";

const withAPI = withProps(() => ({
  updateUsers: (offset, limit, name_filter) =>
    jhapiRequest(
      `/users?include_stopped_servers&offset=${offset}&limit=${limit}&name_filter=${
        name_filter || ""
      }`,
      "GET"
    ).then((data) => data.json()),
  startServer: (name, serverName = "") =>
    jhapiRequest("/users/" + name + "/servers/" + (serverName || ""), "POST"),
  stopServer: (name, serverName = "") =>
    jhapiRequest("/users/" + name + "/servers/" + (serverName || ""), "DELETE"),
  noChangeEvent: () => {
    return null;
  },
}));

export default withAPI;
