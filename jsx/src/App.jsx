import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { compose } from "recompose";
import { initialState, reducers } from "./Store";
import withAPI from "./util/withAPI";
import { HashRouter, Switch, Route } from "react-router-dom";

import ServerDashboard from "./components/ServerDashboard/ServerDashboard";

import "./style/root.css";

const store = createStore(reducers, initialState);

const App = () => {
  return (
    <div className="resets">
      <Provider store={store}>
        <HashRouter>
          <Switch>
            <Route
              exact
              path="/"
              component={compose(withAPI)(ServerDashboard)}
            />
          </Switch>
        </HashRouter>
      </Provider>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("server-manager-hook"));
