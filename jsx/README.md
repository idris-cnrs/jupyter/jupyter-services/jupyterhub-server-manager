# Jupyterhub Server Manager Dashboard - React Variant

This repository contains stripped down version of JupyterHub admin console to
show only running servers of the users. This console will not expose any
user/group related attributes and hence, users that have access to this dashboard
wont be able to modify them.

This is made for the support team at IDRIS HPC center to be able to access user's
servers.

### Build Commands

- `yarn build`: Installs all dependencies and bundles the application
- `yarn hot`: Bundles the application and runs a mock (serverless) version on port 8000
- `yarn lint`: Lints JSX with ESLint
- `yarn lint --fix`: Lints and fixes errors JSX with ESLint / formats with Prettier
- `yarn place`: Copies the transpiled React bundle to /share/jupyterhub/static/js/admin-react.js for use.
