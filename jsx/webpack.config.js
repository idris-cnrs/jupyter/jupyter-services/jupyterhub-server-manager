const webpack = require("webpack");
const path = require("path");

module.exports = {
  entry: path.resolve(__dirname, "src", "App.jsx"),
  mode: "production",
  module: {
    rules: [
      {
        test: /\.(js|jsx)/,
        exclude: /node_modules/,
        use: "babel-loader",
      },
      {
        test: /\.(css)/,
        exclude: /node_modules/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff2?|ttf)$/i,
        exclude: /node_modules/,
        use: "file-loader",
      },
    ],
  },
  output: {
    publicPath: "/",
    filename: "server-manager-react.js",
    path: path.resolve(__dirname, "build"),
  },
  resolve: {
    extensions: [".css", ".js", ".jsx"],
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
  devServer: {
    static: {
      directory: path.resolve(__dirname, "build"),
    },
    port: 9000,
    onBeforeSetupMiddleware: (devServer) => {
      const app = devServer.app;
      // start user server
      app.post("/hub/api/users/*/server", (req, res) => {
        console.log(req.url, req.body);
        res.status(200).end();
      });
      // stop user server
      app.delete("/hub/api/users/*/server", (req, res) => {
        console.log(req.url, req.body);
        res.status(200).end();
      });
    },
  },
};
