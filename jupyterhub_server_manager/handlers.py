# Copyright 2023 IDRIS / jupyter
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import ipaddress
import json
import logging
from functools import wraps
from http.client import responses

from jinja2 import Environment, TemplateNotFound
from jupyterhub import __version__
from jupyterhub.services.auth import HubOAuthenticated
from jupyterhub.utils import url_path_join
from tornado import web

ACCESS_SCOPE = "custom:server-manager-ui"


# Copied from JupyterHub custom-scopes example
# https://github.com/jupyterhub/jupyterhub/blob/5cfc0db0d5272d04f7f9175bb3930f293382b474/examples/custom-scopes/grades.py#L18-L40
def require_scope(scopes):
    """Decorator to require scopes

    For use if multiple methods on one Handler
    may want different scopes,
    so class-level .hub_scopes is insufficient
    (e.g. read for GET, write for POST).
    """
    if isinstance(scopes, str):
        scopes = [scopes]

    def wrap(method):
        """The actual decorator"""

        @wraps(method)
        @web.authenticated
        def wrapped(self, *args, **kwargs):
            self.hub_scopes = scopes
            return method(self, *args, **kwargs)

        return wrapped

    return wrap


class ServerManagerHandler(HubOAuthenticated, web.RequestHandler):
    """View Server Manager page"""

    # Define our custom scope here to protect the resource
    # Only users who have this scope in their roles can access this UI
    hub_scopes = [ACCESS_SCOPE]

    def initialize(self, loader):
        self.loader = loader
        self.env = Environment(loader=self.loader)
        self.template = self.env.get_template("manager.html")
        # Get whitelisted IPs from settings
        self.whitelisted_ips = self.settings.get("whitelisted_ips", [])

    @property
    def log(self):
        return self.settings.get("log", logging.getLogger("tornado.application"))

    @property
    def template_namespace(self):
        user = self.get_current_user()
        json_escaped_user_name = json.dumps(user['name'])[1:-1]
        prefix = self.hub_auth.hub_prefix
        logout_url = url_path_join(prefix, "logout")

        # Starting from JupyterHub 4.1.5, _xsrf tokens are created for each sub path. Thus,
        # our service has a different _xsrf token compared to path /hub/. When we make
        # these API requests, the browser will send _xsrf token corresponding to /hub/ path
        # as requested API call is to path /hub/api/(.*). But the _xsrf token for the service
        # is different and hence, the request fails due to mismatched _xsrf tokens.
        #
        # So we use the OAuth token for the API requests. We should be able to use these
        # tokens both on server and client side. 
        #
        # IMPORTANT: As OAuth tokens are user specific we need to include 'inherit'
        # scope in the oauth_client_allowed_scopes for the service so that OAuth token
        # will inherit all the scopes provided for the user via custom role. If not
        # the token will have atmost the same scope as the normal user will have and 
        # it will not be enough to be able to access other user's servers
        #
        # Ref: https://discourse.jupyter.org/t/making-api-requests-to-hub-from-browser-from-services/25114/3
        ns = dict(
            base_url="/",
            prefix=self.hub_auth.hub_prefix,
            user=user,
            user_name=json_escaped_user_name,
            login_url=self.hub_auth.login_url,
            logout_url=logout_url,
            static_url=self.static_url,
            no_spawner_check=True,
            token=self.hub_auth.get_token(self),
        )
        return ns

    def write_error(self, status_code, **kwargs):
        """render custom error pages"""
        exc_info = kwargs.get('exc_info')
        message = ''
        exception = None
        status_message = responses.get(status_code, 'Unknown HTTP Error')
        if exc_info:
            exception = exc_info[1]
            # get the custom message, if defined
            try:
                message = exception.log_message % exception.args
            except Exception:
                pass

        # Build tamplate ns
        template_ns = {}
        template_ns.update(self.template_namespace)
        template_ns.update(
            dict(
                status_code=status_code,
                status_message=status_message,
                static_url=self.static_url,
                message=message,
                extra_error_html=getattr(self, 'extra_error_html', ''),
                exception=exception,
            )
        )

        self.set_header('Content-Type', 'text/html')
        if isinstance(exception, web.HTTPError):
            # allow setting headers from exceptions
            # since exception handler clears headers
            headers = getattr(exception, 'headers', None)
            if headers:
                for key, value in headers.items():
                    self.set_header(key, value)
            # Content-Length must be recalculated.
            self.clear_header('Content-Length')

        # render_template is async, but write_error can't be!
        # so we run it sync here, instead of making a sync version of render_template

        try:
            html = self.env.get_template('%s.html' % status_code).render(**template_ns)
        except TemplateNotFound:
            self.log.debug("No template for %d", status_code)
            html = self.env.get_template('error.html').render(**template_ns)

        self.write(html)

    @require_scope([ACCESS_SCOPE])
    async def get(self):
        # Get tamplate ns
        template_ns = {}
        template_ns.update(self.template_namespace)

        # First check if remote IP is within whitelisted IPs
        if self.whitelisted_ips:
            remote_ip = self.request.remote_ip
            remote_ip_obj = ipaddress.IPv4Network(remote_ip)
            allowed = any([remote_ip_obj.subnet_of(sn) for sn in self.whitelisted_ips])

            # If remote ip address does not match at least one subnet we
            # deny authentication
            if not allowed:
                msg = (
                    f"user:{template_ns['user_name']} with "
                    f"remote ip:{remote_ip} is not within whitelisted IPs "
                    "(VPN REF2). Access Denied."
                )
                self.log.warning(msg)
                reason = (
                    "You are attempting to access JupyterHub Server "
                    "Manager from outside of VPN REF2. That is not allowed "
                    "sweetheart..."
                )
                raise web.HTTPError(403, reason)

        html = self.template.render(
            **template_ns,
            server_version=f'{__version__}',
            api_page_limit=self.settings["api_page_default_limit"],
        )
        self.write(html)
