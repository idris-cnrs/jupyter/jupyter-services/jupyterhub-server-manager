# Copyright 2023 IDRIS / jupyter
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import binascii
import ipaddress
import logging
import os
import re
import secrets
import sys
from textwrap import dedent

from jinja2 import ChoiceLoader, FileSystemLoader, PrefixLoader
from jupyterhub._data import DATA_FILES_PATH
from jupyterhub.handlers.static import LogoHandler
from jupyterhub.log import CoroutineLogFormatter
from jupyterhub.services.auth import HubOAuthCallbackHandler
from tornado import ioloop, web
from traitlets import (
    Any,
    Bool,
    Bytes,
    Dict,
    Integer,
    List,
    Unicode,
    Union,
    default,
    observe,
    validate,
)
from traitlets.config import Application

from jupyterhub_server_manager.handlers import ServerManagerHandler
from jupyterhub_server_manager.ssl import SSLContext

COOKIE_SECRET_BYTES = (
    32  # the number of bytes to use when generating new cookie secrets
)

HEX_RE = re.compile('^([a-f0-9]{2})+$', re.IGNORECASE)


class ServerManagerService(Application):
    """Main class that runs the application.

    We setup a customized UI page that is based on admin UI to manager user
    servers. Users with appropriate roles and scope can access this service to
    start/stop/access other users servers"""

    classes = [SSLContext]

    flags = Dict(
        {
            'generate-config': (
                {'ServerManagerService': {'generate_config': True}},
                "Generate default config file",
            )
        }
    )

    generate_config = Bool(False, help="Generate default config file").tag(config=True)

    config_file = Unicode("server_manager_config.py", help="Config file to load").tag(
        config=True
    )

    service_prefix = Unicode(
        os.environ.get("JUPYTERHUB_SERVICE_PREFIX", "/services/server-manager/"),
        help="Server manager service prefix",
    ).tag(config=True)

    port = Integer(8890, help="Port this service will listen on").tag(config=True)

    data_files_path = Unicode(DATA_FILES_PATH, help="Location of JupyterHub data files")

    logo_file = Unicode(
        "",
        help="Logo path, can be used to override JupyterHub one",
    ).tag(config=True)

    @default('logo_file')
    def _logo_file_default(self):
        return os.path.join(
            self.data_files_path, 'static', 'images', 'jupyterhub-80.png'
        )

    template_paths = List(
        help="Search paths for jinja templates, coming before default ones"
    ).tag(config=True)

    @default('template_paths')
    def _template_paths_default(self):
        return [
            os.path.join(self.data_files_path, 'server_manager/templates'),
            os.path.join(self.data_files_path, 'custom-templates'),
            os.path.join(self.data_files_path, 'templates'),
            self.data_files_path,
        ]

    cookie_secret = Union(
        [Bytes(), Unicode()],
        help="""The cookie secret to use to encrypt cookies.
        Loaded from the JPY_SERVER_MANAGER_COOKIE_SECRET env variable by default.
        Should be exactly 256 bits (32 bytes).
        """,
    ).tag(config=True, env='JPY_SERVER_MANAGER_COOKIE_SECRET')

    @validate('cookie_secret')
    def _validate_secret_key(self, proposal):
        """Coerces strings with even number of hexadecimal characters to bytes."""
        r = proposal['value']
        if isinstance(r, str):
            try:
                return bytes.fromhex(r)
            except ValueError:
                raise ValueError(
                    "cookie_secret set as a string must contain an even amount "
                    "of hexadecimal characters."
                )
        else:
            return r

    @observe('cookie_secret')
    def _cookie_secret_check(self, change):
        secret = change.new
        if len(secret) > COOKIE_SECRET_BYTES:
            self.log.warning(
                "Cookie secret is %i bytes.  It should be %i.",
                len(secret),
                COOKIE_SECRET_BYTES,
            )

    cookie_secret_file = Unicode(
        "jupyterhub-server-manager-cookie-secret",
        help="File in which we store the cookie secret.",
    ).tag(config=True)

    whitelisted_ips = List(
        Unicode(),
        help="""List of whitelisted IP addresses to access the server manager.
        If this list is defined, server manager will be only accessible from
        only these IP addresses. If a user attempts to access the server manager
        from IP addresses outside of these, a 403 will be raised
        """,
    ).tag(config=True)

    @validate('whitelisted_ips')
    def _validate_whitelisted_ips(self, proposal):
        """Ensure whitelisted_ips are valid IP addresses"""
        ips = proposal['value']
        ip_objs = []
        for ip in ips:
            try:
                ip_objs.append(ipaddress.IPv4Network(ip, strict=False))
            except Exception as err:
                raise Exception(
                    f"Invalid IP address {ip} found in whitelisted_ips. "
                    f"Error is {err}"
                )
        return ips

    whitelisted_ip_objs = Any()

    def init_whitelisted_ips(self):
        """Convert IP address strings to IPv4Network objects"""
        self.whitelisted_ip_objs = [
            ipaddress.IPv4Network(ip, strict=False) for ip in self.whitelisted_ips
        ]

    ssl_context = Any()

    _log_formatter_cls = CoroutineLogFormatter

    @default("log_datefmt")
    def _log_datefmt(self):
        return "%Y-%m-%d %H:%M:%S"

    @default("log_format")
    def _log_format(self):
        return (
            "%(color)s[%(levelname)1.1s %(asctime)s.%(msecs).03d %(name)s "
            "%(module)s:%(lineno)d]%(end_color)s %(message)s"
        )

    def init_secrets(self):
        trait_name = 'cookie_secret'
        trait = self.traits()[trait_name]
        env_name = trait.metadata.get('env')
        secret_file = os.path.abspath(os.path.expanduser(self.cookie_secret_file))
        secret = self.cookie_secret
        secret_from = 'config'
        # load priority: 1. config, 2. env, 3. file
        secret_env = os.environ.get(env_name)
        if not secret and secret_env:
            secret_from = 'env'
            self.log.info("Loading %s from env[%s]", trait_name, env_name)
            secret = binascii.a2b_hex(secret_env)
        if not secret and os.path.exists(secret_file):
            secret_from = 'file'
            self.log.info("Loading %s from %s", trait_name, secret_file)
            try:
                with open(secret_file) as f:
                    text_secret = f.read().strip()
                if HEX_RE.match(text_secret):
                    # >= 0.8, use 32B hex
                    secret = binascii.a2b_hex(text_secret)
                else:
                    # old b64 secret with a bunch of ignored bytes
                    secret = binascii.a2b_base64(text_secret)
                    self.log.warning(
                        dedent(
                            """
                    Old base64 cookie-secret detected in {0}.
                    JupyterHub Server Manager service expects 32B hex-encoded
                    cookie secret for tornado's sha256 cookie signing.
                    To generate a new secret:
                        openssl rand -hex 32 > "{0}"
                    """
                        ).format(secret_file)
                    )
            except Exception as e:
                self.log.error(
                    "Refusing to run JupyterHub Server Manager service with "
                    "invalid cookie_secret_file. "
                    "%s error was: %s",
                    secret_file,
                    e,
                )
                self.exit(1)

        if not secret:
            secret_from = 'new'
            self.log.debug("Generating new %s", trait_name)
            secret = secrets.token_bytes(COOKIE_SECRET_BYTES)

        if secret_file and secret_from == 'new':
            # if we generated a new secret, store it in the secret_file
            self.log.info("Writing %s to %s", trait_name, secret_file)
            text_secret = binascii.b2a_hex(secret).decode('ascii')
            with open(secret_file, 'w') as f:
                f.write(text_secret)
                f.write('\n')
        # store the loaded trait value
        self.cookie_secret = secret

    def initialize(self, argv=None):
        """Initialise the application"""
        super().initialize(argv)

        if self.generate_config:
            print(self.generate_config_file())
            sys.exit(0)

        if self.config_file:
            self.load_config_file(self.config_file)

        self.init_logging()
        self.init_secrets()
        self.init_whitelisted_ips()

        # Set SSL context
        self.init_ssl_context()

        base_path = self._template_paths_default()[0]
        if base_path not in self.template_paths:
            self.template_paths.append(base_path)
        loader = ChoiceLoader(
            [
                PrefixLoader({'templates': FileSystemLoader([base_path])}, '/'),
                FileSystemLoader(self.template_paths),
            ]
        )

        self.settings = {
            "cookie_secret": self.cookie_secret,
            "whitelisted_ips": self.whitelisted_ip_objs,
            "api_page_default_limit": 10,
            "static_path": os.path.join(self.data_files_path, "static"),
            "log": self.log,
        }

        self.app = web.Application(
            [
                (
                    self.service_prefix,
                    ServerManagerHandler,
                    dict(loader=loader),
                ),
                (self.service_prefix + r"oauth_callback", HubOAuthCallbackHandler),
                (self.service_prefix + "logo", LogoHandler, dict(path=self.logo_file)),
            ],
            **self.settings,
        )

    def init_logging(self):
        # This prevents double log messages because tornado use a root logger
        # that self.log is a child of. The logging module dipatches log
        # messages to a log and all of its ancenstors until propagate is set to
        # False.
        self.log.propagate = False

        # disable curl debug, which is TOO MUCH
        logging.getLogger("tornado.curl_httpclient").setLevel(
            max(self.log_level, logging.INFO)
        )

        for name in ("access", "application", "general"):
            # ensure all log statements identify the application they come from
            logger = logging.getLogger(f"tornado.{name}")
            logger.name = self.log.name

        # hook up tornado's and oauthlib's loggers to our own
        for name in ("tornado", "oauthlib"):
            logger = logging.getLogger(name)
            logger.propagate = True
            logger.parent = self.log
            logger.setLevel(self.log.level)

    def init_ssl_context(self):
        self.ssl_context = SSLContext(config=self.config).ssl_context()
        if not self.ssl_context:
            self.log.warning("No SSL context provided")

    def start(self):
        """Start the application"""
        self.app.listen(
            self.port,
            ssl_options=self.ssl_context,
            # For X-Real-IP and X-Forwarded-For to be propagated to the
            # handlers
            xheaders=True,
        )
        ioloop.IOLoop.current().start()


def main():
    """Main entry point"""
    app = ServerManagerService()
    app.initialize()
    app.start()
