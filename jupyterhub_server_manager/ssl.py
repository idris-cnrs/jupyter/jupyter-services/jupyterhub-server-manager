# Copyright 2023 IDRIS / jupyter
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from jupyterhub.utils import make_ssl_context
from traitlets import Unicode
from traitlets.config import Configurable


class SSLContext(Configurable):
    keyfile = Unicode("", help="SSL key, use with certfile").tag(config=True)

    certfile = Unicode("", help="SSL cert, use with keyfile").tag(config=True)

    cafile = Unicode("", help="SSL CA, use with keyfile and certfile").tag(config=True)

    def ssl_context(self):
        if self.keyfile and self.certfile and self.cafile:
            return make_ssl_context(
                self.keyfile, self.certfile, cafile=self.cafile, check_hostname=False
            )
        else:
            return None
