# JupyterHub Server Manager

This service provides a server manager that can start/stop/access user's servers.
This is based on the react admin app of JupyterHub. We strip down all the
unnecessary elements from react app UI console just to expose list of running
servers.

This can be typically used by the support teams to access user's servers without
having access to Admin console.

## Getting started

Install the package using following commands

```
git clone https://gitlab.com/idris-cnrs/jupyter/jupyter-services/jupyterhub-server-manager.git
cd jupyterhub-server-manager
pip install .
```

You can generate the configuration file using

```
jupyterhub_server_manager --generate-config
```

This will generate a default configuration file. An example configuration file
can be

```
# Configuration file for application.

c = get_config()  #noqa

#------------------------------------------------------------------------------
# Application(SingletonConfigurable) configuration
#------------------------------------------------------------------------------
## This is an application.

## The date format used by logging formatters for %(asctime)s
#  Default: '%Y-%m-%d %H:%M:%S'
# c.Application.log_datefmt = '%Y-%m-%d %H:%M:%S'

## The Logging format template
#  Default: '[%(name)s]%(highlevel)s %(message)s'
# c.Application.log_format = '[%(name)s]%(highlevel)s %(message)s'

## Set the log level by value or name.
#  Choices: any of [0, 10, 20, 30, 40, 50, 'DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL']
#  Default: 30
# c.Application.log_level = 30

## Configure additional log handlers.
#
#  The default stderr logs handler is configured by the log_level, log_datefmt
#  and log_format settings.
#
#  This configuration can be used to configure additional handlers (e.g. to
#  output the log to a file) or for finer control over the default handlers.
#
#  If provided this should be a logging configuration dictionary, for more
#  information see:
#  https://docs.python.org/3/library/logging.config.html#logging-config-
#  dictschema
#
#  This dictionary is merged with the base logging configuration which defines
#  the following:
#
#  * A logging formatter intended for interactive use called
#    ``console``.
#  * A logging handler that writes to stderr called
#    ``console`` which uses the formatter ``console``.
#  * A logger with the name of this application set to ``DEBUG``
#    level.
#
#  This example adds a new handler that writes to a file:
#
#  .. code-block:: python
#
#     c.Application.logging_config = {
#         'handlers': {
#             'file': {
#                 'class': 'logging.FileHandler',
#                 'level': 'DEBUG',
#                 'filename': '<path/to/file>',
#             }
#         },
#         'loggers': {
#             '<application-name>': {
#                 'level': 'DEBUG',
#                 # NOTE: if you don't list the default "console"
#                 # handler here then it will be disabled
#                 'handlers': ['console', 'file'],
#             },
#         }
#     }
#  Default: {}
# c.Application.logging_config = {}

## Instead of starting the Application, dump configuration to stdout
#  Default: False
# c.Application.show_config = False

## Instead of starting the Application, dump configuration to stdout (as JSON)
#  Default: False
# c.Application.show_config_json = False

#------------------------------------------------------------------------------
# ServerManagerService(Application) configuration
#------------------------------------------------------------------------------
## This is an application.

## Config file to load
#  Default: 'server_manager_config.py'
# c.ServerManagerService.config_file = 'server_manager_config.py'

## The cookie secret to use to encrypt cookies.
#          Loaded from the JPY_SERVER_MANAGER_COOKIE_SECRET env variable by default.
#          Should be exactly 256 bits (32 bytes).
#  Default: traitlets.Undefined
# c.ServerManagerService.cookie_secret = traitlets.Undefined

## File in which we store the cookie secret.
#  Default: 'jupyterhub-server-manager-cookie-secret'
# c.ServerManagerService.cookie_secret_file = 'jupyterhub-server-manager-cookie-secret'

## Generate default config file
#  Default: False
# c.ServerManagerService.generate_config = False

## The date format used by logging formatters for %(asctime)s
#  See also: Application.log_datefmt
# c.ServerManagerService.log_datefmt = '%Y-%m-%d %H:%M:%S'

## The Logging format template
#  See also: Application.log_format
# c.ServerManagerService.log_format = '[%(name)s]%(highlevel)s %(message)s'

## Set the log level by value or name.
#  See also: Application.log_level
# c.ServerManagerService.log_level = 30

##
#  See also: Application.logging_config
# c.ServerManagerService.logging_config = {}

## Logo path, can be used to override JupyterHub one
#  Default: ''
# c.ServerManagerService.logo_file = ''

## Port this service will listen on
#  Default: 8890
# c.ServerManagerService.port = 8890

## Server manager service prefix
#  Default: '/services/server-manager/'
# c.ServerManagerService.service_prefix = '/services/server-manager/'

## Instead of starting the Application, dump configuration to stdout
#  See also: Application.show_config
# c.ServerManagerService.show_config = False

## Instead of starting the Application, dump configuration to stdout (as JSON)
#  See also: Application.show_config_json
# c.ServerManagerService.show_config_json = False

## Search paths for jinja templates, coming before default ones
#  Default: []
# c.ServerManagerService.template_paths = []

#------------------------------------------------------------------------------
# SSLContext(Configurable) configuration
#------------------------------------------------------------------------------
## SSL CA, use with keyfile and certfile
#  Default: ''
# c.SSLContext.cafile = ''

## SSL cert, use with keyfile
#  Default: ''
# c.SSLContext.certfile = ''

## SSL key, use with certfile
#  Default: ''
# c.SSLContext.keyfile = ''

```

## Running service

We preferably run this service as externally managed service. We can do it by
various means, however, we always need to make sure that following environment
variables are set

```
# Client ID that we need to set
JUPYTERHUB_CLIENT_ID=service-server-manager

# Service prefix
JUPYTERHUB_SERVICE_PREFIX=/services/server-manager/

# Set Hub API URL
JUPYTERHUB_API_URL=http://localhost:8081/hub/api

# API token that we used in the services definition in the jupyterhub_config.py
# NOTE THAT IT SHOULD ABSOLUTELY MATCH WITH THE ONE PROVIDED IN JUPYTERHUB CONFIG
JUPYTERHUB_API_TOKEN=secret-token
```

These variables need to be appropriately modified based on JupyterHub deployment.
Once the above environment variables are set, service can be run using

```
jupyterhub_server_manager --config=server_manager.py
```

assuming all the config is placed in `server_manager.py` file.


## Configuration


We need to first create a custom scope in JupyterHub that is being used by the
service.

```
c.JupyterHub.custom_scopes = {
    "custom:server-manager-ui": {
        "description": "Access server manager UI console to start/stop/access user servers",
    },
}
```

We need to create a role and assign that role to target users/groups.

```
c.JupyterHub.load_roles = [
    {
        "name": "server-manager",
        "users": ["foo001", "foo002"],
        "scopes": [
            "custom:server-manager-ui",
            "access:services!service=server-manager",
            "list:users",
            # start/stop servers for users in the class
            "admin:servers",
            # access servers for users in the class
            "access:servers",
        ],
    },
]

```

Finally we need to register the service with JupyterHub as we will run this
as external service. It is important to add `inherit` scope in the `oauth_client_allowed_scopes`. 
Else, the generated OAuth token will not have custom scopes we defined in the 
`load_roles` for the users.

```
c.JupyterHub.services = [
    {
        'name': 'server-manager',
        'url': 'http://localhost:8890',
        'api_token': 'secret-token',
        'oauth_no_confirm': True,
        'oauth_client_allowed_scopes': [
            'custom:server-manager-ui', 'inherit'
        ],
    }
]

```
