# Configuration file for jupyterhub.
import os

c.JupyterHub.authenticator_class = 'jupyterhub.auth.DummyAuthenticator'


def default_url_fn(handler):
    user = handler.current_user
    if user and user.admin:
        return '/hub/admin'
    return '/hub/home'


c.JupyterHub.default_url = default_url_fn

c.JupyterHub.custom_scopes = {
    "custom:server-manager-ui": {
        "description": "Access server manager UI console to start/stop/access user servers",
    },
}

c.JupyterHub.load_roles = [
    # Users to access server manager service
    {
        "name": "server-manager",
        "users": ["foo001", "foo002"],
        "scopes": [
            # Read users list
            "list:users",
            # start/stop servers for users in the class
            "admin:servers",
            # access servers for users in the class
            "access:servers",
            # Access server manager UI console
            "custom:server-manager-ui",
            # Access server manager service
            "access:services!service=server-manager",
        ],
    },
]

api_token = 'my8digitsecret'
c.JupyterHub.services = [
    {
        'name': 'server-manager',
        'url': 'http://localhost:8890',
        'api_token': api_token,
        'oauth_no_confirm': True,
        'oauth_client_allowed_scopes': [
            'custom:server-manager-ui',
        ],
    }
]

c.Authenticator.admin_users = ['admin']
