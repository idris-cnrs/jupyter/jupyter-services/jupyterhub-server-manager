"""Integration tests"""

import requests


def test_endpoints():
    """Test to check if server manager URL is working"""
    # Auth data
    payload = {
        'username': 'foo001',
        'password': 'pass',
    }
    # Open a session to login and check announcement URL
    with requests.Session() as session:
        login = session.get("http://localhost:8000/hub/login")
        assert login.status_code == 200

        # Get XSRF session cookie and update payload
        cookie = login.cookies['_xsrf']
        payload.update({'_xsrf': cookie})

        # Test auth
        auth = session.post("http://localhost:8000/hub/login", data=payload)
        assert auth.status_code == 200

        # Test service endpoint
        manager = session.get("http://localhost:8000/services/server-manager/")
        assert manager.status_code == 200
